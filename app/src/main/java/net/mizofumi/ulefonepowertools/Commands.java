package net.mizofumi.ulefonepowertools;

import android.app.Activity;
import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

/**
 * Created by mizof on 2016/05/10.
 */
public class Commands {

    public String sudo1(String command) throws Exception {
        String output = "";
        try{
            Runtime runtime = Runtime.getRuntime();
            Process su = runtime.exec("su");



            DataOutputStream outputStream = new DataOutputStream(su.getOutputStream());

            //outputStream.writeBytes("screenrecord --time-limit 10 /sdcard/MyVideo.mp4\n");
            outputStream.writeBytes(command);
            outputStream.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(su.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                output += line + "\n";
            }
            reader.close();


            outputStream.writeBytes("exit\n");
            outputStream.flush();
            su.waitFor();

            return output;
        }catch(IOException e){
            throw new Exception(e);
        }catch(InterruptedException e){
            throw new Exception(e);
        }
    }

    public static void sudo2(String...strings) {
        try{
            Process su = Runtime.getRuntime().exec("su");
            DataOutputStream outputStream = new DataOutputStream(su.getOutputStream());

            for (String s : strings) {
                outputStream.writeBytes(s+"\n");
                outputStream.flush();
            }

            outputStream.writeBytes("exit\n");
            outputStream.flush();
            try {
                su.waitFor();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            outputStream.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public static String sudo(String command){
        Process p;
        StringBuffer output = new StringBuffer();
        try {
            p = Runtime.getRuntime().exec(command);
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(p.getInputStream()));
            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
                p.waitFor();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (output.length() != 0)
            return output.toString().substring(0,output.length()-1);
        return "";
    }

    public static void assetCopyToLocalStorage(Activity activity,AssetManager assetManager,String filename){
        try {
            InputStream inputStream = assetManager.open(filename);
            FileOutputStream fileOutputStream = activity.openFileOutput(filename, Activity.MODE_PRIVATE);
            byte[] buffer = new byte[1024];
            int length = 0;
            while ((length = inputStream.read(buffer)) >= 0) {
                fileOutputStream.write(buffer, 0, length);
            }
            fileOutputStream.close();
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void remount(){
        ///dev/block/platform/mtk-msdc.0/by-name/system
        Process suProcess;
        DataOutputStream os;
        try{
            //Get Root
            suProcess = Runtime.getRuntime().exec("su");
            os= new DataOutputStream(suProcess.getOutputStream());

            //Remount writable FS within the root process
            os.writeBytes("mount -w -o remount -t ext4 /dev/block/platform/mtk-msdc.0/by-name/system /system\n");
            os.flush();

            //Do something here
            //os.writeBytes("rm /system/somefile\n");
            //os.flush();

            //Do something there
            //os.writeBytes("touch /system/somefile\n");
            //os.flush();

            //Remount Read-Only
            //os.writeBytes("mount -r -o remount -t ext4 /dev/block/platform/mtk-msdc.0/by-name/system /system\n");
            //os.flush();

            //End process
            os.writeBytes("exit\n");
            os.flush();

        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
