package net.mizofumi.ulefonepowertools;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.stericson.RootTools.RootTools;
import com.stericson.RootTools.exceptions.RootDeniedException;
import com.stericson.RootTools.execution.Command;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class CameraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);


        (findViewById(R.id.cameraon)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraon();
                Toast.makeText(CameraActivity.this,"カメラ音を有効化しました",Toast.LENGTH_SHORT).show();
            }
        });

        (findViewById(R.id.cameraoff)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraoff();
                Toast.makeText(CameraActivity.this,"カメラ音を無効化しました",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void cameraoff(){
        RootTools.remount("/system", "rw");
        try {
            RootTools.runShellCommand(RootTools.getShell(true), new Command(0,"am force-stop com.android.gallery3d") {
                @Override
                public void commandOutput(int i, String s) {

                }

                @Override
                public void commandTerminated(int i, String s) {

                }

                @Override
                public void commandCompleted(int i, int i1) {

                }
            });
            RootTools.runShellCommand(RootTools.getShell(true), new Command(0,"mv /system/media/audio/ui/camera_click.ogg /system/media/audio/ui/bk_camera_click.ogg") {
                @Override
                public void commandOutput(int i, String s) {

                }

                @Override
                public void commandTerminated(int i, String s) {

                }

                @Override
                public void commandCompleted(int i, int i1) {

                }
            });
            RootTools.runShellCommand(RootTools.getShell(true), new Command(0,"mv /system/media/audio/ui/camera_focus.ogg /system/media/audio/ui/bk_camera_focus.ogg") {
                @Override
                public void commandOutput(int i, String s) {

                }

                @Override
                public void commandTerminated(int i, String s) {

                }

                @Override
                public void commandCompleted(int i, int i1) {

                }
            });
            RootTools.runShellCommand(RootTools.getShell(true), new Command(0,"mv /system/media/audio/ui/camera_shutter.ogg /system/media/audio/ui/bk_camera_shutter.ogg") {
                @Override
                public void commandOutput(int i, String s) {

                }

                @Override
                public void commandTerminated(int i, String s) {

                }

                @Override
                public void commandCompleted(int i, int i1) {

                }
            });
            RootTools.runShellCommand(RootTools.getShell(true), new Command(0,"mv /system/media/audio/ui/VideoRecord.ogg /system/media/audio/ui/bk_VideoRecord.ogg") {
                @Override
                public void commandOutput(int i, String s) {

                }

                @Override
                public void commandTerminated(int i, String s) {

                }

                @Override
                public void commandCompleted(int i, int i1) {

                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (RootDeniedException e) {
            e.printStackTrace();
        }
    }

    private void cameraon(){
        RootTools.remount("/system", "rw");
        try {
            RootTools.runShellCommand(RootTools.getShell(true), new Command(0,"am force-stop com.android.gallery3d") {
                @Override
                public void commandOutput(int i, String s) {

                }

                @Override
                public void commandTerminated(int i, String s) {

                }

                @Override
                public void commandCompleted(int i, int i1) {

                }
            });
            RootTools.runShellCommand(RootTools.getShell(true), new Command(0,"mv /system/media/audio/ui/bk_camera_click.ogg /system/media/audio/ui/camera_click.ogg") {
                @Override
                public void commandOutput(int i, String s) {

                }

                @Override
                public void commandTerminated(int i, String s) {

                }

                @Override
                public void commandCompleted(int i, int i1) {

                }
            });
            RootTools.runShellCommand(RootTools.getShell(true), new Command(0,"mv /system/media/audio/ui/bk_camera_focus.ogg /system/media/audio/ui/camera_focus.ogg") {
                @Override
                public void commandOutput(int i, String s) {

                }

                @Override
                public void commandTerminated(int i, String s) {

                }

                @Override
                public void commandCompleted(int i, int i1) {

                }
            });
            RootTools.runShellCommand(RootTools.getShell(true), new Command(0,"mv /system/media/audio/ui/bk_camera_shutter.ogg /system/media/audio/ui/camera_shutter.ogg") {
                @Override
                public void commandOutput(int i, String s) {

                }

                @Override
                public void commandTerminated(int i, String s) {

                }

                @Override
                public void commandCompleted(int i, int i1) {

                }
            });
            RootTools.runShellCommand(RootTools.getShell(true), new Command(0,"mv /system/media/audio/ui/bk_VideoRecord.ogg /system/media/audio/ui/VideoRecord.ogg") {
                @Override
                public void commandOutput(int i, String s) {

                }

                @Override
                public void commandTerminated(int i, String s) {

                }

                @Override
                public void commandCompleted(int i, int i1) {

                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (RootDeniedException e) {
            e.printStackTrace();
        }
    }
}
