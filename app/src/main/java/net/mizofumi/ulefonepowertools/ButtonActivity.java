package net.mizofumi.ulefonepowertools;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.stericson.RootTools.RootTools;
import com.stericson.RootTools.exceptions.RootDeniedException;
import com.stericson.RootTools.execution.Command;

import net.mizofumi.ulefonepowertools.Fragment.PlaceholderFragment;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class ButtonActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new AlertDialog.Builder(ButtonActivity.this)
                        .setTitle("確認")
                        .setMessage("適用してもよろしいですか？(再起動します)")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                int page = mViewPager.getCurrentItem();
                                switch (page){
                                    case 0:
                                        changeDefaultButton();
                                        break;
                                    default:
                                        changeButton(page);
                                        break;
                                }
                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .show();


            }
        });

    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
                case 3:
                    return "SECTION 4";
            }
            return null;
        }
    }

    public void changeDefaultButton(){
        Commands.assetCopyToLocalStorage(this,getResources().getAssets(),"Generic_DEF.kl");
        RootTools.remount("/system", "rw");
        try {
            RootTools.runShellCommand(RootTools.getShell(true), new Command(0,"mv /data/data/net.mizofumi.ulefonepowertools/files/Generic_DEF.kl /system/usr/keylayout/Generic_DEF.kl") {
                @Override
                public void commandOutput(int i, String s) {

                }

                @Override
                public void commandTerminated(int i, String s) {

                }

                @Override
                public void commandCompleted(int i, int i1) {

                }
            });
            RootTools.runShellCommand(RootTools.getShell(true), new Command(0,"mv /system/usr/keylayout/Generic_DEF.kl /system/usr/keylayout/Generic.kl") {
                @Override
                public void commandOutput(int i, String s) {

                }

                @Override
                public void commandTerminated(int i, String s) {

                }

                @Override
                public void commandCompleted(int i, int i1) {

                }
            });
            RootTools.runShellCommand(RootTools.getShell(true), new Command(0,"chmod 644 /system/usr/keylayout/Generic.kl") {
                @Override
                public void commandOutput(int i, String s) {

                }

                @Override
                public void commandTerminated(int i, String s) {

                }

                @Override
                public void commandCompleted(int i, int i1) {

                }
            });
            RootTools.runShellCommand(RootTools.getShell(true), new Command(0,"chown root:root /system/usr/keylayout/Generic.kl") {
                @Override
                public void commandOutput(int i, String s) {

                }

                @Override
                public void commandTerminated(int i, String s) {

                }

                @Override
                public void commandCompleted(int i, int i1) {

                }
            });
            RootTools.runShellCommand(RootTools.getShell(true), new Command(0,"reboot") {
                @Override
                public void commandOutput(int i, String s) {

                }

                @Override
                public void commandTerminated(int i, String s) {

                }

                @Override
                public void commandCompleted(int i, int i1) {

                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (RootDeniedException e) {
            e.printStackTrace();
        }
    }
    public void changeButton(int num){
        String number = String.valueOf(num);
        Commands.assetCopyToLocalStorage(this,getResources().getAssets(),"Generic"+number+".kl");
        RootTools.remount("/system", "rw");
        try {
            RootTools.runShellCommand(RootTools.getShell(true), new Command(0,"mv /data/data/net.mizofumi.ulefonepowertools/files/Generic"+number+".kl /system/usr/keylayout/Generic"+number+".kl") {
                @Override
                public void commandOutput(int i, String s) {

                }

                @Override
                public void commandTerminated(int i, String s) {

                }

                @Override
                public void commandCompleted(int i, int i1) {

                }
            });
            RootTools.runShellCommand(RootTools.getShell(true), new Command(0,"mv /system/usr/keylayout/Generic"+number+".kl /system/usr/keylayout/Generic.kl") {
                @Override
                public void commandOutput(int i, String s) {

                }

                @Override
                public void commandTerminated(int i, String s) {

                }

                @Override
                public void commandCompleted(int i, int i1) {

                }
            });
            RootTools.runShellCommand(RootTools.getShell(true), new Command(0,"chmod 644 /system/usr/keylayout/Generic.kl") {
                @Override
                public void commandOutput(int i, String s) {

                }

                @Override
                public void commandTerminated(int i, String s) {

                }

                @Override
                public void commandCompleted(int i, int i1) {

                }
            });
            RootTools.runShellCommand(RootTools.getShell(true), new Command(0,"chown root:root /system/usr/keylayout/Generic.kl") {
                @Override
                public void commandOutput(int i, String s) {

                }

                @Override
                public void commandTerminated(int i, String s) {

                }

                @Override
                public void commandCompleted(int i, int i1) {

                }
            });
            RootTools.runShellCommand(RootTools.getShell(true), new Command(0,"reboot") {
                @Override
                public void commandOutput(int i, String s) {

                }

                @Override
                public void commandTerminated(int i, String s) {

                }

                @Override
                public void commandCompleted(int i, int i1) {

                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (RootDeniedException e) {
            e.printStackTrace();
        }
    }
}
